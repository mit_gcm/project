Je ne peux que travailler le mercredi après midi généralement

# Liens utiles

## Youtube
* [Playlist](https://youtu.be/kKrNHIgTV5k) pour ArcGIS en général avec sélection par attributs ou location (J'en suis à la vidéo 8)
* Deux vidéos pour python - ArcMap/ArcGIS : [première](https://youtu.be/iWzF1px-xuo), [deuxième](https://youtu.be/U9uVSBLpWFk)
* [Vidéo très claire pour l'add-in/toolbox](https://youtu.be/OW505NE2SWg)

# Commentaires importants

* Notation: GB pour référer à Guy & Boutay (Pour indiquer leurs travaux)

* Features to polygons -> Rend pleins de polygônes. Or je ne veux pas tous les polygones pour séparer océan et continent
* Aggregate Polygons -> fusionne trop de polygônes. Je n'en veux uniquement les "gros".
Ceux qui englobent les autres sans prendre par exemple tout le rectangle du cadre.
* Eliminate Polygon Part (je ne pouvais pas utiliser Eliminate de Data Management après le Features to polygons car c'est un shapefile):  

	-Le "percentage" fait des erreurs.

	-Le "AREA" ne fait pas d'erreurs mais ça ne me semble pas très "portable" pour différentes échelles. Et pour l'instant, je ne comprends pas pourquoi avec "AREA" 10 milles, j'obtiens, si je garde la couleur olive une carte sans certains îles alors qu'en changeant le "fill color" en "no color", j'ai toutes les îles. J'ai aussi vérifié que le nombre de polygones dans la table est pareil.

* Classification par Histogramme pour "niveaux de gris" ? -> Reclassify (Spatial Analyst Tool) ou Con

* Trouvé que Markdown doit utiliser .PNG au lieu de .png et .jpg au lieu de .JPG .. 

* Très important pour ModelBuilder: L'ordre dans lequel on rajoute des outils est important.


# BUTS COURANTS

* Rapport, optimisation et rajout d'une colonne gardé les numéros de points initiaux

* Autres.

# Historique:
## 4/03/2020:
* Lecture rapide du rapport des prédecesseurs et de leurs codes
* Obtention de la licence de ArcGIS Advanced 10.6 (doute si je l'ai activé mais en tout cas ça marche)
* Première prise en main du logiciel ArcGIS.
* Essai de l'utilisation du add-in et du toolbox créé par les prédecesseurs: conclusion temporaire:
    
	-Il y a bien des fenêtres qui s'ouvrent pour le add-in après tout les étapes dans le rapport de GB (Guy & Boutay pour raccourcir)

	-On voit bien des endroits pour insérer des noms de fichiers

	-Il n'y a pas d'images pour les boutons du plug-in et le plugin ne semble pas marcher. (Il y avait une erreur si je m'en souviens bien, à vérifier plus tard)

## 11/03/2020:
* Un peu de manipulation des tables avec les sélections par attributs/ select by attributes (je n'ai pas encore réussi pour la sélection : select by location mais je ne crois pas que ça soit important pour le moment, possible que ça soit utile si j'arrive à le manipuler pour résoudre le problème). J'ai un cours en parallèle avec SQL donc ça aide.
Comment exporter en gdb pour database ou en shapefile. 
* Le "definition query" dans "properties" du p. ex shapefile pour pouvoir juste montrer à l'écran les sélections (à partir de SQL) (Je n'ai pas encore testé sur les fichiers que vous avez donné car je verrai plus tard comment bien sélectionner les bons polygônes et créer les bons attributs.)
* Features to polygons -> Rend pleins de polygônes. Or je ne veux pas tous les polygones pour séparer océan et continent
* Aggregate Polygons -> fusionne trop de polygônes. Je n'en veux uniquement les "gros".
Ceux qui englobent les autres sans prendre par exemple tout le rectangle du cadre.

* Eliminate Polygon Part (je ne pouvais pas utiliser Eliminate de Data Management après le Features to polygons car c'est un shapefile):  
    -Le "percentage" fait des erreurs.
    -Le "AREA" ne fait pas d'erreurs mais ça ne me semble pas très "portable" pour différentes échelles. Et pour l'instant, je ne comprends pas pourquoi avec "AREA" 10 milles, j'obtiens, si je garde la couleur olive une carte sans certains îles alors qu'en changeant le "fill color" en "no color", j'ai toutes les îles. J'ai aussi vérifié que le nombre de polygones dans la table est pareil.
* Check geometry : juste au cas où.

* Création du README (donc de l'historique).

Différentes questions actuelles:

	- Donc, votre problème était pour Feature to Polygon. Mais j'ai oublié ce que le cadre aidait pour ?

	- Je voudrai juste à l'écrit ce que vous faites manuellement étapes par étapes pour pouvoir séparer océan du continent. Cela me permettra de ne pas oublier ou de mal interpréter.

	- Est-ce que je devrais "merge" les tous petits polygones ensembles ? (Vu que vous vouliez la classification océan/continent).

	- "Le "AREA" ne fait pas d'erreurs mais ça ne me semble pas très "portable" pour différentes échelles. Et pour l'instant, je ne comprends pas pourquoi avec "AREA" 10 milles, j'obtiens, si je garde la couleur olive une carte sans certains îles alors qu'en changeant le "fill color" en "no color", j'ai toutes les îles. J'ai aussi vérifié que le nombre de polygones dans la table est pareil." Si vous avez une idée je suis prenant.

## 12/03/2020:
* Réception des informations supplémentaires (de Christian Vérard):\
\
Pour créer un « cadre », je crée un shapefile sous arcCatalog (cf. Img-01_NewShapeFile.jpg). Je lui donne un nom (par exemple Cadre), je dis que c’est une polyligne, et je lui donne un système de coordonnées (par ex. WGS_1984) (cf. Img-02_CreationCadre.jpg).
\
J’ouvre ce shapefile sous arcMap, et je démarre son édition (cf. Img-03_StartEditCadre.jpg). Dans la zone de dessin, je clique sur le bouton droit de la souris et je donne les valeurs absolues des coordonnées des nœuds de mon cadre (cf. Img-04_CreateAbsoluteCoordinates.jpg) ; soit -180°/+90° ; +180°/+90° ; +180°/-90° ; et -180°/-90° (cf. Img-05_FinCreationCadre.jpg).
\
Pourquoi faire ce cadre ?
Si j’ai un contour quelconque (par ex. la ligne rouge dans Img-06_ContourEtCadre.jpg), je veux en fait fermer cette ligne rouge pour en faire un polygone. Il y a plusieurs possibilités. L’une est d’éditer les nœuds de la ligne rouge, et continuer la ligne jusqu’au nœud suivant. C’est ce qu’ont essayé vos prédécesseurs. L’autre est d’utiliser une autre polyligne – le cadre (vert) – pour la combiner au contour (rouge), de manière à « facilement » fermer les lignes.
J’ai pensé que c’était plus facile, mais en fait, ça ne marche pas forcément.
\
Mais logiquement, en utilisant l’outils Feature To Polygon, l’idée est d’utiliser ces deux lignes (i.e. la ligne de contour et la ligne du cadre) pour créer un polygon (cf. Img-07_AllPolygon.jpg).
\
Ensuite, il faut malgré tout – à la main – effacer la partie de polygone qui correspond à l’océan pour ne laisser que le continent (cf. Img-08_DeletePolygonParts.jpg).
La question est : que faire de la partie de polygone à l’intérieur du continent. Vous mentionnez le fait d’utiliser une taille critique pour soit l’effacer (et laisser donc un trou), soit utiliser la fonction merge pour le fondre avec les autres parties de polygone en un seul polygone.
En fait, je ne pense pas que ce soit une question de taille, mais plutôt une question de profondeur. On pourrait chercher si un pixel du raster appartenant à ce polygone est plus profond que ( ??? valeur à définir) -2000 m, alors la zone appartient à l’océan et on efface le polygone, sinon, ça appartient au continent, et on le « merge » dans un polygone principal.

## 16/03/2020:
* Rajout des images importantes pour la suite du projet (dans InformationsImportantes\PolygonManualClassification)
* Idée de classification avec niveaux de gris. Utilisation de l'outil "classified": (du Layer) Properties->Symbology->Classified->Classify
* On peut choisir plusieurs "Method" et j'ai essayé "Quantile". Cela classifie grâce à l'histogramme. Cela ne reste que de l'affichage.
* Rajout des images dans: Classification\Quantile
* Questions : Peut-on utiliser des outils pour obtenir des polygones de cette façon ?

* Trouvé que Markdown doit utiliser .PNG au lieu de .png et .jpg au lieu de .JPG .. 
* Idée très prometteuse: utiliser justement la profondeur pour séparer océan et terre.
* Essai avec Reclassify (Spatial Analyst Tool) en appliquant les mêmes procédures pour classifier mais manuellement avec le niveau de l'océan voulu
* Obtention de 2 classes, l'une au dessus et l'autre en dessous du niveau -> pouvons-nous faire mieux et ne pas prendre les lacs ? en comparant les valeurs minimales dans les "polygones"
* Pouvons-nous en obtenir des polygones ?
* OUI ! on peut obtenir des polygones, ils ne sont pas fusionnés mais c'est classifié !

* Comment ? Raster -> Times (x100 pour avoir des integers) -> Reclassify -> Raster To Polygons et ensuite on peut faire des sélections dessus ! 

## 17/03/2020
* Rajout de cette image:
* Résultat si on exporte la sélection par attribut Gridcode 2 (par Definition Query):
![](./Classification/Manual/Resultat1.PNG)

* A voir si possible de le faire en code et aussi d'enlever les lacs.

## 18/03/2020
* [Vidéo très claire pour l'add-in/toolbox](https://youtu.be/OW505NE2SWg)
* Création de l'add-in avec 2 outils (non fonctionnels pour le moment car aucun lien avec un code "extérieur")
* Création du toolbox.
* "Liage" du toolbox avec l'add-in avec os.path.dirname et [GPToolDialog(toolbox, tool_name)](https://desktop.arcgis.com/fr/arcmap/latest/analyze/python-addins/the-pythonaddins-module.htm).
* Add-in avec Contour "fonctionnel" (la fenêtre s'ouvre)
* Update: l'outil est fonctionnel. Il faut faire attention à changer la direction : input/output de l'outil dans le toolbox -> voir le rapport de G&B
* Rajout de Raster à Polygones classifiés (Gridcode avec valeurs 0 ou 1, 1 pour au dessus du niveau de l'océan).
* N'efface pas les polygones, n'en combinent pas non plus. Les fusionner plus tard et aussi résoudre le problème des lacs.

## 19/03/2020
Exportation en shp uniquement pour AuDessus (niveau de la mer) et un autre shp pour EnDessous.

1) J'applique sur AuDessus une union ce qui rempli les trous (il faudra fusionner des polygones etc. plus tard).
2) Appliquer une exportation en shp sur le FID_Export=-1 (les trous sélectionnés)
3) Select by location des Trous avec EnDessous avec "are identical to the source layer feature"
4) Edition du EnDessous (Edit) -> Suppression de "l'intersection des trous avec EnDessous" avec "Delete".

On a donc AuDessus_union et EnDessous deux features class correspondant à l'océan et le continent

* Solution pour le problème trouvé ! Il ne reste plus qu'à mettre dans le code.

## 25/03/2020
* Outil fonctionnel (Juste que je n'ai pas gérer les signaux pour que cela efface les fichiers/dossiers temporaires..)
* L'outil ne marche pas sur l'ordinateur de Christian Vérard. Plusieurs solutions ont été essayé mais en vain.

## 16/04/2020
* Apprentissage de Model Builder. Peut être que cela résoudra.
Si cela ne marche pas, il y a peu de chances que le projet aboutisse si ce sont des problèmes de fichiers sur son ordinateur.
La résolution de problème à distance..
* Le modèle marche sur mon ordinateur. Attente que Christian Vérard l'essaie.
* Est-ce qu'il y a un moyen de lier l'add-in (avec les boutons) au modèle ?
* Le modèle exporté en script ne marche pas. Problème avec un outil spécifique au ModelBuilder

![](./Model/ExportGraphic.png)

## 18/04/2020
* Modification du modèle car préconditions fausses lorsqu'on a des nombres négatifs comme entrée.
* L'outil semble ne pas avoir d'erreurs.
* Le modèle est dans le toolbox, je n'ai pas encore mis à jour les scripts dans les autres dossiers.
* Le modèle marche.
* Remarque: J'ai utilisé "Con" au lieu de "Reclassify". "Con" est un outil permettant de faire la classification binaire simplement avec une expression SQL au lieu de passer par Remap.
* Très important pour ModelBuilder: L'ordre dans lequel on rajoute des outils est important.

![](./Model/NouveauModeleClassification.png)
![](./Images/RasterToClassifiedPolygons1.PNG)
![](./Images/RasterToClassifiedPolygons2.PNG)

## 20/04/2020
* L'outil marche dans l'add-in (sur mon ordinateur, à attendre la réponse pour l'ordinateur du client)
* Il fallait modifier MITgcmPlus_addin pour "pointer" sur l'outil à l'intérieur du Toolbox.
* Réception des informations supplémentaires du client:\
"
Pour aller plus loin, je rappelle que l’idée de départ est de faire un outil qui nous permette de définir le ruissellement (runoff) sur nos topographies du passé (paleoDEM) à partir de la grille (cubed-sphere ; CS) utilisé pour le modèle de climat (MITgcm). Vos prédécesseurs avaient construit cette fenêtre (image ci-jointe) : Il faudrait avoir quelque chose comme ça en effet à la fin.
\
Donc, l’idée est effectivement d’avoir des bouton pour :
\
·         charger une grille quelconque (e.g. Browse a given CS grid).
\
·         charger une topography (e.g. Browse a given paleoDEM).
\
·         donner un niveau marin (e.g. set a given sea-level).
\
Et quand on clique « calculate », l’outil calcule :
\
·         le polygon du continent (déjà réussi)
\
·         utilise l’outil Selection d’arcGIS et crée une nouvelle colonne dans la GS Grid où on inscrit, par exemple, 1 pour les points contenu dans le polygon, et 0 pour les points hors du polygon.
Ensuite, ça devient difficile… Il faut relier chaque point continental (=1) de la CS Grid à un point de l’océan (=0) le plus proche de l’embouchure d’un fleuve. Il y a deux stratégies : soit l’outil fait le calcul de l’hydrologie, ce qui serait plus « juste » car l’hydrologie dépend du niveau marin choisi ; soit on utilise des cartes d’hydrologie dàjà faites précédemment (avec un niveau marin très bas ; par exemple -333 m) plus ou moins valables pour tous les niveaux marins que l’utilisateur peut utiliser avec l’outil.
\
Mais bon, je ne sais pas si on pourra aller jusque-là. Pour l’instant, si vous pouvez créer un add-in qui ouvre une fenêtre du type de l’image ci-jointe, avec un bouton qui fasse déjà marcher la partie faite avec le ModelBuilder, ce serait bien.
"
![](./InformationsImportantes/ImageOutilPredecesseur.png)

* De plus, "la grille sphéri-cubique est une série de points (de coordonnées) qui est utilisé dans MITgcm". Comme entrée de l'outil, on a déjà un shapefile avant de lancer l'outil (voir dossier Grid).
* topographie (paleoDEM): raster layer
* Basin : Inconnu pour le moment car pas dans la liste des priorités.

Voici à quoi ressemble la grille:
![](./Grid/grille.PNG)

Je dois donc rajouter une colonne (attribut) pour indique si le point est à l'intérieur ou non du polygône.

Seconde information:\
"
Les attributs de la grille sont simples (au départ) ; il ne faut que X, Y, et un numéro correspondant à la position du point dans le modèle MIT (le numéro mis dans le Shape file et le fichier XL en exemple n’est d’ailleurs pas le bon, mais c’est pas grave ; ce n’est pas utilisé).
Ensuite, deux options : soit on ajoute une colonne « continent » vide dans les attributs quand on crée cette grille, soit votre outil le fait. Ensuite, il faudrait que votre outil sélectionne les points de la grille qui sont inclus dans le polygône et attribue une valeur (par ex. =1) à ces points et une autre (par ex. =0) aux autres, comme le fait l’outil « Select by location » d’arcGIS (cf. image ci-dessous).
"
![](./InformationsImportantes/SelectionByLocationGridPoints/SelectByLocation.png)

Voici la sélection (Sans mon outil, mon outil le fait sans le montrer):
![](./Images/SelectByLocation.PNG)

* Attention, s'il y a une erreur dans le config.xml, on ne pourra pas installer

* Pour créer un nouveau outil dans l'add-in, il faut rajouter du code dans MITgcmPlus_addin.py (Mettre un numéro à la place de `<Num>`):
~~~~
class ButtonClass<Num>(object):
    """Implementation for MITgcmPlus_addin.button_<Num> (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False
    def onClick(self):
        # Calling the toolbox
        pythonaddins.GPToolDialog(toolboxPath, "NomDuScriptOuModele") 
	
~~~~

* Et il faut aussi modifier dans le config.xml dans la section commande:
~~~~
<Button caption="NomDuScriptOuModele" category="MITgcmPlus" class="ButtonClass<Num>" id="MITgcmPlus_addin.button_<Num>" image="" message="" tip="NomDuScriptOuModeleOuMessage"><Help heading="" /></Button>
~~~~
* Mais aussi dans le toolbar:
~~~~
<Button refID="MITgcmPlus_addin.button_<Num>" />
~~~~

* Le fichier MITgcmPlus_addin.py lie les modèles grâce aux noms des modèles, MITgcmPlus_addin.py est lié avec config.xml grâce au nom des classes python.

* Cet outil pour la modification d'attributs dans la grille fonctionne sur mon ordinateur.

* Schéma/Modèle:

![](./Model/ClassifyGridModel.png)
* Deux images:
![](./Images/ClassifyGridMyTool1.PNG)
![](./Images/ClassifyGridMyTool2.PNG)

## 21/04/2020
* Nouvelles informations:
* J’ai installé l’add-in et testé.
La première fois, avec des fichiers sur mon bureau, tout a très bien fonctionné.
Mais la seconde fois, avec les mêmes fichiers placés dans un dossier avec un chemin plus long, j’ai eu des erreurs, pour la création du polygône, et pour la classification (addition d’une colonne) de la grille CS. Je pense donc que si le nom du chemin est trop long, ça pose problème (cf. images). Pouvez-vous corriger ça ?
\
\
A partir d’une topographie (par ex. le raster pDEM250nncor que vous avez [et outils d’hydrology (Spatial Analyst Tools/Hydrology)]), il faut :
\
-Extraire la topographie continentale, c’est-à-dire appeler l’outil Spatial Analyst Tools/Extraction/Extract by Mask. Le raster en input est la topographie de départ (par ex. le raster pDEM250nncor), le masque est le polygône qu’on a généré, et j’appelle généralement le raster de sortie Land_... quelque chose.
\
Ce raster Land contient des points bas et/ou des « micro-trous » qui empêchent un calcul des rivières jusqu’à la mer. Il faut donc utiliser l’outil  Analyst Tools/Hydrology/Fill, avec en entrée le raster Land, et en sortie un raster que j’appelle généralement Fill_... quelque chose (je n’utilise pas l’option Z limit).
\
-Calculer la direction de flux, c’est-à-dire appeler l’outil Spatial Analyst Tools/Hydrology/Flow Direction, avec en entrée le raster Fill, et en sortie un raster que j’appelle généralement FlwDir_... quelque chose. Je coche l’option « Force all edge cells to flow outward”; je n’utilise pas l’option “output drop raster”; et je prends l’option par défaut D8 comme « Flow direction type ».
\
J’avoue que pour ces trois dernières options, je ne sais pas ce qui est le mieux d’utiliser. Il me semble que le type D8 est plus rapide d’une part et est de toutes façons nécessaire pour le calcul des bassins (ci-après), et l’option « Force all edge cells to flow outward » me semble éviter que des rivières s’arrêtent au milieu de nulle part, même si ça pose des problèmes, notamment au pôle nord et au pôle sud. Mais bon, bref…
\
Calculer les basins enfin, c’est-à-dire appeler l’outil Spatial Analyst Tools/Hydrology/Basin, avec en entrée le raster FlwDir, et en sortie un raster que j’appelle généralement Bs_... quelque chose.

* L’idée est que votre outil fasse toutes ces étapes tout seul sans que l’utilisateur ait besoin à chaque fois de choisir les fichiers en entrée et en sortie. Les fichiers créés ne seront que temporaires. Le dernier outil (« Basin ») crée un raster de bassins versants, dont chaque bassin a une valeur, i.e. un numéro de 1 à …généralement beaucoup (par ex. 50000).
 
Une fois ceci fait, il faut à nouveau créé une colonne au shape file de points de la gille CS, et y indiquer le numéro du bassin dans lequel le point se trouve. Pour cela, j’appelle l’outil Spatial Analyst Tools/Extraction/Extract Value to Point, avec en entrée le shape file de points de la gille CS, et le raster Bs, et en sortie, on obtient un nouveau shape file de points de la gille CS (je n’utilise pas les options « interpolate… » ni « append… ».
"
* Problèmes fixés: c'était les lock files et le nom du fichier qui contenait un -.

## 23/04/2020
* Les étapes qui suivent les premiers points pour obtenir les basins. Pour la dernière étape avec la colonne, pas encore essayé.
* D'abord on va extraire du raster le continent uniquement en utilisant la couche polygône qu'on a créé plus tôt:
![](./InformationsImportantes/Basin/ExtractByMask.PNG)
![](./InformationsImportantes/Basin/ExtractByMask2.PNG)
* Puis on remplir les micro-trous avec Fill:
![](./InformationsImportantes/Basin/Fill.PNG)
![](./InformationsImportantes/Basin/Fill2.PNG)
* Puis calculer le Flow direction (avec la méthode D8, à voir les différences plus tard):
![](./InformationsImportantes/Basin/FlowDirection.PNG)
![](./InformationsImportantes/Basin/FlowDirection2.PNG)
* Puis finalement utiliser l'outil Basin:
![](./InformationsImportantes/Basin/Basin.PNG)
![](./InformationsImportantes/Basin/Basin2.PNG)
* Une image en couleur:
![](./InformationsImportantes/Basin/BasinColor.PNG)

* Dernière étape avec l'ajout de la colonne pour les bassins en utilisant Extract Values To Points:
![](./InformationsImportantes/GridPointsAddBasin/ExtractValuesToPoints.PNG)
![](./InformationsImportantes/GridPointsAddBasin/ExtractValuesToPoints2.PNG)

* Création du modèle:
![](./Model/BasinPointsModel.png)

* On peut facilement combiner des modèles ensemble dans un modèle plus grand.

## 24/04/2020
* Réception des informations:\
Comme je l’ai dit, la dernière étape est plus délicate : Relier les points d’un bassin (BsPts) à un point de l’océan (OcPts).
\
Jusque-là, je faisais quelque chose d’assez compliqué, qu’on doit pouvoir améliorer.
\
Ce que je faisais :
\
Je transformais la ligne de contour en points (shape file que j’appelais CoastPts) avec DataManagementTool/Feature/FeatureVerticeToPoints. Avec SpatialAnalystTools/Extraction/ExtractMultiValueToPoint, j’attribuais un numéro de bassin à ces CoastPts (à partir du raster des bassins), ainsi qu’une valeur de FlowAccumulation (à partir du raster FlwAcc). Sous Excel, je ne conservais qu’un seul point par bassin, le point dont la valeur FlwAcc est la plus grande.
\
Ainsi, j’avais sur le trait de côte et pour chaque bassin le point (CoastPt) le plus « représentatif » de l’embouchure de la rivière.
\
Ensuite j’utilisais des « joints ». D’abord, un joints entre les points des bassins (BsPts) et les CoastPts en utilisant leur numéro de bassin; en clair, pour chaque ligne du tableau de BsPts sont ajoutées les colonnes du tableau des CoastPts quand le numéro du bassin est le même dans BsPts et CoastPts.
\
Puis j’utilisais l’outil AnalysisTool/Proximity/Near pour qu’à chaque CoastPts, je trouve le point de l’océan OcPts le plus proche. Je trouvais ainsi le point de l’océan qui reçoit l’eau des rivières de chaque bassin versant.
\
Enfin, j’utilisais un nouveau « joints ». Pour chaque BsPts, je joignais l’OcPts qui avait le même CoastPts.
\
Compliqué, non ?
\
Ce qu’on doit pouvoir faire :
\
Je pense qu’on peut faire sauter l’étape qui utilise les CoastPts (cf. aussi image ci-jointe).
\
Dans l’étape précédente, lorsqu’on donne un numéro de bassin à chaque point de la grille CS32, il faut aussi donner (dans une seconde colonne) la valeur du raster FlowAccumulation avec SpatialAnalystTools/Extraction/ExtractMultiValueToPoint (donc sur le raster Basins et le raster FlwAcc). Ensuite, il faut créer une troisième colonne (avec des 0 & 1, ou du texte ‘Embouchure’ & ‘_’) où, pour tous les points qui ont le même numéro de bassin, on attribue une valeur (1 ou ‘RiverMouth’) au point qui a la plus grande valeur de FlwAcc.
\
A ce stade, je ne sais pas exactement quelle est la marche à suivre. J’imagine qu’il faut sélectionner les points océaniques, c’est-à-dire les points dont le numéro de bassin est -9999, et en faire un fichier shape file OcPts. Faire de même avec les points continentaux, c’est-à-dire les points dont le numéro de bassin est différent de -9999 (ou >=0), et en faire un fichier shape file ConPts.
\
Avec le fichier de points ConPts, il faut utiliser l’outil AnalysisTool/Proximity/Near pour trouver le point de l’océan (OcPts) le plus proche. Le problème est que chaque ConPts aura un OcPts le plus proche différent (avec un numéro différent). Nous, nous voulons attribuer à tous les points (ConPts) qui ont le même numéro de bassin, le numéro et les attributs (surtout les coordonnées X & Y) du OcPts qui correspond au ConPt qui a la valeur ‘RiverMouth’ (ou 1). En clair, tous les points avec le même numéro de bassin doivent avoir des colonnes supplémentaires dont les attributs seront tous identiques et correspondront aux valeurs du OcPt lié à ‘RiverMouth’.


## 26/04/2020
* Apprentissage superficielle des itérateurs dans ModelBuilder
* Apprentissage des outils Summarize Statistics pour obtenir le maximum du FlowAcc, utilisation des jointures entre tables (avec RASTERVALU comme attribut de jointure).
* Modification du modèle Basin pour inclure la colonne avec les FlowAcc (Flow accumulation).

![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/MaybeFalseJoinTableAfterSummarizeStatisticsUsingRastervalueAndFlowAcc.PNG)

* Remarque: on peut avoir plusieurs points ayant le même FlowAcc pour un bassin.
![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/MaybeFalseManyFlowAccMaxForOneBasin.PNG)

* Remarque: je me suis probablement trompé. Voici le schéma d'avant, je l'ai appliqué sur le fill..:
![](./Model/MaybeFalseBasinUpdate.png)

* Après les modifications:

![](./Model/BasinUpdate.png)

![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/MaybeCorrect.PNG)
![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/JoinTableAfterSummarizeStatisticsUsingRastervalueAndFlowAcc.PNG)

* Avec des couleurs et les bassins:
![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/AnotherImage.PNG)

* Avec FlowLen minimisé en plus. Après export de la sélection d'avant puis summarize de nouveau et jointure
![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/AnotherImage2.PNG)
![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/ExportThenLaterMinOnFlowLen.PNG)
* Summarize statistics sur FlowLen en minimisant au lieu de maximiser (On veut le plus court allant jusqu'à une sortie)
![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/SummarizeFlowLenMin.PNG)
* Jointure avec RASTERVALU de nouveau
![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/JoinFlowLen.PNG)
* Resultat. Il faudra encore utiliser l'outil Near plus tard.
![](./InformationsImportantes/MaxFlowAccSummarizeStatistics/ResultWithoutNear.PNG)

* Informations supplémentaires:\
Alors il faut faire le calcul du point d’océan le plus proche (outil Near), et considérer que le point correspondant à l’exutoire (l’embouchure) est le point qui a non seulement la plus grande valeur de FlowAcc, mais aussi la plus petite valeur de Near.
\
Ou alors, ce qui serait mieux : pour sélectionner le point de l’embouchure avant de faire « near », il faut rajouter des choses aux étapes précédentes.
\
Dans l’étape où on calcule l’hydrologie (FlwDir, FlwAcc…), il faut également ajouter le calcul d’un raster de Flow Length (outil Spatial Analyst Tool/Hydrology/Flow Length, avec en entrée toujours le même fichier FlwDir et en sortie un fichier FlwLgth…quelquechose (et option, la direction de mesure par défaut : « downstream », et pas d’ « input weight raster »).
\
Dans l’étape Spatial Analyst Tool/Extraction/ExtractMultiValueToPoint, rajouter une colonne supplémentaire à la grille CS avec la valeur de FlwLgth. C’est une information de toutes façons utiles que j’aurais dû vous demander d’indiquer depuis le début (je l’ai oubliée).
\
Et donc, dans le choix du point correspondant à l’embouchure, prendre le point qui a non seulement la plus grande valeur de FlowAcc, mais aussi la plus petite valeur de FlwLgth (c’est-à-dire dont la distance à l’exutoire est la plus courte).
\
Ensuite, on pourra utiliser l’outil Near pour trouver le point de l’océan le plus proche de ce point précis. Je pense que c’est mieux comme ça.

## 27/04/2020
* Outil:

* Remarque: Attention, il faut à chaque fois supprimer les flow acc/len dans le scratch gdb et renommer l'output field name dans les extract multi values to point sinon, il crée un nouveau nom à chaque fois qu'on lance (en tout cas depuis run entire model).
![](./Model/BasinUpdate2Result.PNG)
* Autre remarque: l'application crash ou a des erreurs si on lance en dehors du run entire model. A voir pourquoi.

## 29/04/2020
* Crash quasi constant de l'outil.. même après 20-30 essais..
* Causes (déduction à partir des essais..) des problèmes: 
* Dans run entire model: Le nom des attributs est uniquement généré donc il faut effacer le scratchGDB
* En dehors: crash constant de l'application quand on lance mais pas tout de suite. C'est vers le Extract multivalues de FlowLen.
* Erreur au lieu du crash complet si on ne met dans une géodatabase
* Ouverture d'un outil dans le modèle (Extract multivalues de FlowLen ou FlowAcc plus précisément)

* Voici l'erreur SANS CRASH en dehors d'une géodatabase pour sortie:
\
ERROR 000728: Field FlowAcc does not exist within table
\
Failed to execute (Summary Statistics (Max FlowAcc)).
\
Failed at Wed Apr 29 22:34:49 2020 (Elapsed Time: 0,04 seconds)
\
Failed to execute (BasinPoints).
\
Failed at Wed Apr 29 22:34:50 2020 (Elapsed Time: 53,12 seconds)
* J'ai arrêté de compter combien de crash depuis.. plusieurs heures d'essais
* En dehors de l'outil dans une géodatabase:
![](./Errors/Crash.PNG)
* En dehors de l'outil en dehors des géodatabases:
![](./Errors/BasinPointDehors.PNG)
![](./Errors/BasinPointDehors2.PNG)

## 30/04/2020
* Trouvé une solution qui semble toujours marcher pour le moment:
* Si on voit la ScratchGDB, il faut la supprimer et actualiser les géodatabases après être de l'outil
Bien sûr il ne faut pas lancer depuis edit mode..
* Remarque: Join field est extrêment lent entre une table en dehors du scratchGDB et les tables à l'intérieur.

## 1/05/2020
* Rajout d'éléments dans le modèle et modification pour FlowLen.
* Fixé le problème de sélection du minimum FlowLen = FlowLen car problème de float vs double
* Changement pour le calcul du minimum pour FlowLen car il était calculé par rapport au numéro de bassin uniquement, il fallait en plus que ça soit des points ayant le MaxFlowAcc pour chaque bassin.
* Donc rajout de sélection.

* Le min FlowLen est toujours lié aux MaxFlowAcc car j'ai fait la sélection par dessus.
* Rajout de RiverMouth (1 pour minFlowLen et MaxFlowAcc, au fait on n'a plus besoin de maxflowacc car on MinFlowLen était basé dessus)

## 2/05/2020
* La jointure dans NearPoints était sur FID, je l'ai changé sur RASTERVALU donc on peut obtenir directement le point de l'océan sur la même ligne.
* Rajout de quelques éléments intermédiaires en paramètres pour pouvoir les utiliser plus tard (les modèles que je montre juste après (sauf le modèle final, Point, RiverMouth et NearPoints) ont l'ancienne image donc sans les paramètres supplémentaires)
* Les différents modèles sont montrés dans les sous-dossiers de Model (RunoffModel)
* Modèle final (ou presque si on veut supprimer des colonnes):
![](./Model/RunoffModel/RunoffModel.png)
* Résultat (Temps pris sur mon ordinateur 3min40):
![](./Model/RunoffModel/Result.PNG)
* Modèle RasterToClassifiedPolygons:
![](./Model/RunoffModel/RasterToClassifiedPolygons/RasterToClassifiedPolygons.png)
* Modèle Points:
![](./Model/RunoffModel/Points/BasinWithEverythingFinalVersion.png)

* A quoi ressemble "l'interface":
![](./Model/RunoffModel/Interface.PNG)
* ATTENTION: Ne pas mettre la sortie de défaut donnée dans l'image, il ne faut pas avoir Output Point features dans une géodatabase !! Sinon, l'outil rate sur Make Table View (lors d'une requête pour sélectionner les points du Continent ayant le FlowAcc maximal) : invalid expression (pour une raison encore inconnue pour le moment)
* D'ailleurs, on remarque qu'on peut sauvegarder le polygône et le raster bassin.

## Les sous-modèles de Points:
* BasinPoints: Rajoute des informations à propos du FlowLen et FlowAcc et RASTERVALU qui est le numéro du bassin.
![](./Model/RunoffModel/Points/BasinPoints/BasinPoints.png)
* MaxFlowAcc: Rajoute le Max_FlowAc
![](./Model/RunoffModel/Points/MaxFlowAcc/BasinPointsSubModel3.png)
* MinFlowLenWithSelection: Rajoute le MinFlowLen mais basé sur Max_FlowAc par sélection. Attention ici, FlowLen c'est minFlowLen..
![](./Model/RunoffModel/Points/MinFlowLenWithSelection/MinFlowLenWithSelection.png)
* MinFlowLenFloat: Puisque la colonne était en Double, on crée une autre colonne en Float et on efface celle en Double
![](./Model/RunoffModel/Points/MinFlowLenDoubleToFloat/MinFlowLenFloat.png)
* RiverMouth: On rajoute la colonne indiquant si c'est un RiverMouth.
![](./Model/RunoffModel/Points/RiverMouth/RiverMouth.png)
* NearPoints: Rajoute les informations sur le point Océan le plus proche des RiverMouth. Puisque la jointure était sur RASTERVALU, on a directement le point océan lié à un point dans un certain Bassin.
![](./Model/RunoffModel/Points/NearPoints/NearPoints.png)

## 3/05/2020
* L'outil ne marche pas sur l'ordinateur du client
* Déduction en voyant les images des modèles sur son ordinateur: problème lié aux sous-modèles
* Possiblité de solutions ? 1) Store relative path dans les propriétés des modèles. 2 ) Créer un modèle contenant tout
* Essaie pour 1) Attention de réponse

## 4/05/2020
* Essai de mise à modèle global (en copiant coller mes sous-modèles et en reliant les bons paramètres si je ne me suis pas trompé..)
![](./Model/GlobalModelNotWorking.png)
* Erreur: (De base j'avais mis en sous-modèle car ça m'évitait des erreurs inconnues mais aussi c'est plus claire/compréhensible et plus facile à corriger des problèmes)
![](./Errors/GlobalModelError.PNG)

## Après
* Le modèle global marche avec la première solution.
* Le modèle était un tout petit peu différent ce que le client voulait car ça avait été spécifié à l'envers l'utilisation du maxflowacc et minflowlen. La modification semble marcher.
* Rapport en cours
* Possiblité d'optimiser ? Join Field extrêmement lent sur l'ordinateur du client. Prend environ 20 minutes à chaque fois et pour moi 1 min
* A rajouter de le rapport l'intuition sur ce qu'on veut faire et les informations sur ce que c'est un Raster. Quelques manipulations de tables rapidement