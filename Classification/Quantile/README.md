# Classifications:

## (du Layer) Properties->Symbology->Classified->Classify :

### Method Quantile:

Avec 3 classes:
<img src="classificationQuantile3Classes.PNG" width="800" />

Résultat:
![Resultat](./classificationQuantile3ClassesImage.PNG)

Comparaison avec le contour reçu:

![Comparaison](./classificationComparison.PNG)
