# Reclassify using Spatial Analyst Tool

Pouvons-nous faire mieux ? Sans les lacs par exemple ?

Note : Je n'ai pas mis la bonne hauteur (je l'ai déplacée manuellement avec l'histogramme).
Mais ce sera dans un add-in.

Comment ça marche:

* Multiplier les valeurs du raster par 100 en utilisant Times (pour permettre l'utilisation des autres outils)
Cela permet ensuite de revenir vers les valeurs voulues.
Attention ! Pour le niveau d'élévation de l'haut derrière l'interface (dans le code) je multiplie par 100 !
Etant donné qu'on ne réutilise pas les données de hauteur dans les polygones, ce n'est pas grave si je ne redivise pas par 100.
Si on veut revenir à la précision de départ, on devrait rediviser.

* Reclassifier le Raster en utilisant le bon niveau d'élévation !

![](./ReclassifyRaster.PNG)

On obtient un nouveau raster avec des "labels"

* Ensuite il faut transformer ce raster en polygone:

![](./RasterToPolygon.PNG)

On aura pleins de polygones (plusieurs centaines) mais au moins on a les labels !
On pourra essayer de fusionner ensemble plusieurs polygones dans le futur mais d'abord il faut éliminer les lacs !

* En utilisant SQL, on peut facilement sélectionner les polygones dans une classe ou une autre !

![](./SelectByAttribute.PNG)
![](./SwitchedSelection.PNG)

* Résultat si on exporte la sélection par attribut Gridcode 2:
![](./Resultat1.PNG)