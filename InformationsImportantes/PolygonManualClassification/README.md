# Transformation en polygone manuellement

## Création du cadre
<img src="./Img-01_NewShapeFile.jpg" width="800" />

![1](./Img-02_CreationCadre.jpg)


![2](./Img-03_StartEditCadre.jpg)

![3](./Img-04_CreateAbsoluteCoordinates.jpg)

![4](./Img-05_FinCreationCadre.jpg)

## Problématique

![5](./Img-06_ContourEtCadre.jpg)

![6](./Img-07_AllPolygon.jpg)

![7](./Img-08_DeletePolygonParts.jpg)

![8](./Img-09_FinalPolygon.jpg)