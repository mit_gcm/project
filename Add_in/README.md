# Création Add-in & Toolbox

[Vidéo très claire](https://youtu.be/OW505NE2SWg)

* Création de l'outil fonctionnel

* Attention, s'il y a une erreur dans le config.xml, on ne pourra pas installer

* Pour créer un nouveau outil dans l'add-in, il faut rajouter du code dans MITgcmPlus_addin.py (Mettre un numéro à la place de `<Num>`):
~~~~
class ButtonClass<Num>(object):
    """Implementation for MITgcmPlus_addin.button_<Num> (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False
    def onClick(self):
        # Calling the toolbox
        pythonaddins.GPToolDialog(toolboxPath, "NomDuScriptOuModele") 
	
~~~~

* Et il faut aussi modifier dans le config.xml dans la section commande:
~~~~
<Button caption="NomDuScriptOuModele" category="MITgcmPlus" class="ButtonClass<Num>" id="MITgcmPlus_addin.button_<Num>" image="" message="" tip="NomDuScriptOuModeleOuMessage"><Help heading="" /></Button>
~~~~
* Mais aussi dans le toolbar:
~~~~
<Button refID="MITgcmPlus_addin.button_<Num>" />
~~~~

* Le fichier MITgcmPlus_addin.py lie les modèles grâce aux noms des modèles, MITgcmPlus_addin.py est lié avec config.xml grâce au nom des classes python.
* 