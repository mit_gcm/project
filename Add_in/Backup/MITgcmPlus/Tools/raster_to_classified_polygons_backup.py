# Copyright (c) [2020] [Stephane Liem Nguyen]
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


# coding: utf-8

"""
Raster to classified polygons:

Transform a raster into polygons with classified attributes. Only 2 classes,
one above the sea level, one under the sea level.
"""


# Import first the arcpy functions for python
import arcpy, os
from arcpy import env
from arcpy.sa import *
import pythonaddins

import tempfile, shutil

#https://docs.python.org/3/library/tempfile.html
# Need to manually cleanup.
TMPpath= tempfile.mkdtemp(prefix="TMP",dir=os.path.dirname(__file__))

    
################################ Fetching the parameters ################################

# To fetch the parameters from the tool, you need to use the arcpy.GetParameterAsText()
# function. The number inside the brackets indicates which parameter you will get.
# You fetch the input parameters and the output parameters the same way.

inRaster = arcpy.GetParameterAsText(0) # Raster.
sea_level = arcpy.GetParameterAsText(1) # Sea level as baseContour
outputPolygons = arcpy.GetParameterAsText(2) # For polygons

# For raster to classified polygons,
# we set the raster, sea_level and outputPolygons using the inputs (and output)
# parameters of the tool and we manually set the contour interval.

# Check extension.
arcpy.CheckOutExtension("Spatial")

################################ Calling the functions ################################
# I follow the same procedure as described in my gitlab (French)
# https://gitlab.unige.ch/mit_gcm/project/-/tree/master/Classification%2FManual

#-------------------------------------------------------------------------------------
# Multiply by 1000 to be able to use the other functions because the other functions
# need "integer raster" as input.
outTimes = Times(inRaster, 100) # https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/times.htm
# Don't save it .

ELEVATION=int(float(sea_level)*100) # sea_level * 100

#-------------------------------------------------------------------------------------
# Reclassify the outTimes using elevation/sea_level -> Still a raster
# https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/reclassify.htm

# Before doing it, we need the minimum and maximum values of the raster:
#https://gis.stackexchange.com/questions/175352/how-to-get-max-and-min-from-a-raster-using-arcpy
minRaster = arcpy.GetRasterProperties_management(outTimes, "MINIMUM").getOutput(0) # careful it's still multiplied by 100.
maxRaster = arcpy.GetRasterProperties_management(outTimes, "MAXIMUM").getOutput(0)

# Because it has a problem of , instead of . for float..
# https://community.esri.com/thread/170917

minRaster=int(float(minRaster.replace(',','.'))  )#-757661
maxRaster=int(float(maxRaster.replace(',','.'))  )#503611



# More informations :https://desktop.arcgis.com/fr/arcmap/10.3/tools/data-management-toolbox/get-raster-properties.htm
# https://desktop.arcgis.com/fr/arcmap/latest/tools/spatial-analyst-toolbox/reclassify.htm
# Now reclassify using RemapRange:
outReclass = Reclassify(outTimes, "Value", 
                         RemapValue([[minRaster,ELEVATION, 0],[ELEVATION,maxRaster, 1]])) #Only 2 classes

# Class 1 : Upper the sea-level.

# Need to modify this later for temporary files with new names each time..
# In case of conflict..
outputTmp = os.path.join(TMPpath, "TMP.shp")

# Now change it into polygons using Raster to Polygon:
arcpy.RasterToPolygon_conversion(outReclass, outputTmp, "NO_SIMPLIFY")

# instead outputPolygons, use temporary files. Need to remove all of them
# after finishing.

# "Cleaning polygons", creating one layer of continent.
#arcpy.AddMessage("Workspace is {0}, exporting to {1}".format(arcpy.env.workspace,outDir))

# Select then export in another temporary file.
# https://pro.arcgis.com/fr/pro-app/tool-reference/data-management/select-layer-by-attribute.htm
# Not the tool I want !!
# arcpy.SelectLayerByAttribute_management(outputTmp, 'NEW_SELECTION', '"gridcode"=1')


#https://stackoverflow.com/questions/1557351/python-delete-non-empty-dir
# Extremely dangerous, better to remove one by one later.
shutil.rmtree(TMPpath,ignore_errors=True)
arcpy.AddMessage("Removed TMP directory")

