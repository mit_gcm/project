# Copyright (c) [2020] [Stephane Liem Nguyen]
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


# coding: utf-8

"""
Raster to classified polygons:

Transform a raster into polygons with classified attributes. Only 2 classes,
one above the sea level, one under the sea level.
"""


# Import first the arcpy functions for python
import arcpy, os
from arcpy import env
from arcpy.sa import *
import pythonaddins

import tempfile, shutil

arcpy.AddMessage("IMPORTANT: Please don't interrupt even if it takes a long time.")

#https://docs.python.org/3/library/tempfile.html
# Need to manually cleanup.
TMPpath= tempfile.mkdtemp(prefix="TMP",dir=os.path.dirname(__file__))

# Temporary files
outputTmp = os.path.join(TMPpath, "TMPPolygon.shp") # for raster to polygon
outLayerTmp = os.path.join(TMPpath, "TMPLayer.shp") # For Feature class to Feature Layer.
outputUnionTmp = os.path.join(TMPpath, "TMPUnion.shp") # For Union

arcpy.AddMessage("Created TMP Folder and files. If error or interruption, please remove them manually")
################################ Fetching the parameters ################################

# To fetch the parameters from the tool, you need to use the arcpy.GetParameterAsText()
# function. The number inside the brackets indicates which parameter you will get.
# You fetch the input parameters and the output parameters the same way.

inRaster = arcpy.GetParameterAsText(0) # Raster.
sea_level = arcpy.GetParameterAsText(1) # Sea level as baseContour
outputPolygons = arcpy.GetParameterAsText(2) # For polygons

# For raster to classified polygons,
# we set the raster, sea_level and outputPolygons using the inputs (and output)
# parameters of the tool and we manually set the contour interval.

# Check extension.
arcpy.CheckOutExtension("Spatial")

################################ Calling the functions ################################
# I follow the same procedure as described in my gitlab (French)
# https://gitlab.unige.ch/mit_gcm/project/-/tree/master/Classification%2FManual

#-------------------------------------------------------------------------------------
# Multiply by 1000 to be able to use the other functions because the other functions
# need "integer raster" as input.
outTimes = Times(inRaster, 100) # https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/times.htm
# Don't save it .

ELEVATION=int(float(sea_level)*100) # sea_level * 100

#-------------------------------------------------------------------------------------
# Reclassify the outTimes using elevation/sea_level -> Still a raster
# https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/reclassify.htm

# Before doing it, we need the minimum and maximum values of the raster:
#https://gis.stackexchange.com/questions/175352/how-to-get-max-and-min-from-a-raster-using-arcpy
minRaster = arcpy.GetRasterProperties_management(outTimes, "MINIMUM").getOutput(0) # careful it's still multiplied by 100.
maxRaster = arcpy.GetRasterProperties_management(outTimes, "MAXIMUM").getOutput(0)

# Because it has a problem of , instead of . for float..
# https://community.esri.com/thread/170917

minRaster=int(float(minRaster.replace(',','.'))  )#-757661
maxRaster=int(float(maxRaster.replace(',','.'))  )#503611



# More informations :https://desktop.arcgis.com/fr/arcmap/10.3/tools/data-management-toolbox/get-raster-properties.htm
# https://desktop.arcgis.com/fr/arcmap/latest/tools/spatial-analyst-toolbox/reclassify.htm
# Now reclassify using RemapRange:
arcpy.AddMessage("0/5: Reclassifying Modified Raster..")
outReclass = Reclassify(outTimes, "Value", 
                         RemapValue([[minRaster,ELEVATION, 0],[ELEVATION,maxRaster, 1]])) #Only 2 classes

arcpy.AddMessage("1/5: Reclassifying Modified Raster.. DONE")
# Class 1 : Upper the sea-level.


# Now change it into polygons using Raster to Polygon:
arcpy.AddMessage("1/5: Reclassified Raster to Polygons..")
arcpy.RasterToPolygon_conversion(outReclass, outputTmp, "NO_SIMPLIFY")
arcpy.AddMessage("2/5: Reclassified Raster to Polygons.. DONE")
# instead outputPolygons, use temporary files. Need to remove all of them
# after finishing.

# "Cleaning polygons", creating one layer of continent.

# outLayerTmp is a feature layer
arcpy.AddMessage("2/5: Feature class to Feature layer..")
arcpy.MakeFeatureLayer_management(outputTmp, outLayerTmp, '"gridcode"=1') # only over the ocean level 
arcpy.AddMessage("3/5: Feature class to Feature layer.. DONE")

#arcpy.SelectLayerByAttribute_management(outputTmp, 'NEW_SELECTION', '"gridcode"=1')
arcpy.AddMessage("3/5: Union/Removing Gaps..")
arcpy.Union_analysis(outLayerTmp, outputUnionTmp, gaps=False) #no GAPS !!
arcpy.AddMessage("4/5: Union/Removing Gaps..DONE")

# Aggregate polygons (We will lose the gridcode column)
# Dissolve(in_features, out_feature_class, {dissolve_field}, {statistics_fields}, {multi_part}, {unsplit_lines})
arcpy.AddMessage("4/5: Dissolving polygons..")
arcpy.Dissolve_management(outputUnionTmp, outputPolygons)
arcpy.AddMessage("5/5: Dissolving polygons/Merging.. DONE")

#https://stackoverflow.com/questions/1557351/python-delete-non-empty-dir
# Extremely dangerous, better to remove one by one later.
shutil.rmtree(TMPpath,ignore_errors=True)
try: 
    os.rmdir(TMPpath) 
    arcpy.AddMessage("Removed TMP directory")
except OSError as error: 
    arcpy.AddMessage("Cannot remove TMP directory")


# I need to do this later: https://docs.python.org/3/library/signal.html
# in case of signals.. need to remove the folder.

