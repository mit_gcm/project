# Note importante:

* Seul contour a été fait par les prédécesseurs.
* Les autres outils sont des modèles donc pas besoin d'avoir de scripts.
* raster_to_classified_polygons ne marche pas sur l'ordinateur du client donc il n'est plus utilisé dans l'add-in/toolbox
