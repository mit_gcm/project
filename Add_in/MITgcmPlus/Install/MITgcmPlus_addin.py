# Copyright (c) [2020] [Stephane Liem Nguyen]
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
import arcpy
import pythonaddins

import os
# Link the toolbox with the add-in using relative path.
# The add-in or the toolbox shouldn't be moved somewhere else.
relativePath = os.path.dirname(__file__)
toolboxPath = os.path.join(relativePath, "MITgcmPlus.tbx")

# Each class can be a button or combox etc.

class ButtonClass(object):
    """Implementation for MITgcmPlus_addin.button (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False
    def onClick(self):
        # Calling the toolbox
        pythonaddins.GPToolDialog(toolboxPath, "Contour")
        
class ButtonClass2(object):
    """Implementation for MITgcmPlus_addin.button_1 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False
    def onClick(self):
        # Calling the toolbox
        pythonaddins.GPToolDialog(toolboxPath, "RasterToClassifiedPolygons")

class ButtonClass3(object):
    """Implementation for MITgcmPlus_addin.button_2 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False
    def onClick(self):
        # Calling the toolbox
        # the correct name is really important
        pythonaddins.GPToolDialog(toolboxPath, "ClassifyGrid")

class ButtonClass4(object):
    """Implementation for MITgcmPlus_addin.button_3 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False
    def onClick(self):
        # Calling the toolbox
        # the correct name is really important
        pythonaddins.GPToolDialog(toolboxPath, "RunoffModel")

class ButtonClass5(object):
    """Implementation for MITgcmPlus_addin.button_4 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False
    def onClick(self):
        # Calling the toolbox
        # the correct name is really important
        pythonaddins.GPToolDialog(toolboxPath, "AddCSNo")
        

class ComboBoxClass(object):
    """Implementation for MITgcmPlus_addin.combobox (ComboBox)"""
    def __init__(self):
        self.items = ["item1", "item2"]
        self.editable = True
        self.enabled = True
        self.dropdownWidth = 'WWWWWW'
        self.width = 'WWWWWW'
    def onSelChange(self, selection):
        pass
    def onEditChange(self, text):
        pass
    def onFocus(self, focused):
        pass
    def onEnter(self):
        pass
    def refresh(self):
        pass
